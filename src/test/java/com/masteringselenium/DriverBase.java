package com.masteringselenium;

import com.masteringselenium.listeners.ScreenshotListener;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Listeners(ScreenshotListener.class)
public class DriverBase {

    public static final String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyz0123456789";
    private String currentTestName;
    protected RemoteWebDriver driver;

    boolean profile_created = false;
    public String user_login = "pedaku";
    public String user_mail = "zkssadmidsdastng@gmail.com";
    public String password = "kudlzks";
    @BeforeClass
    public void setup() throws MalformedURLException {
        driver = getDriver();


        if (!profile_created) {
            String user_firstname = "Peta";
            String user_lastname = "Kubelka";

            driver.get("http://demo.redmine.org");

            //V mem prohlizeci je pouzita ceska lokalizace, mozna zamenit za cestinu za aj
            //TODO
            String textLogin = "Registrovat";
            //String textLogin = "Sign in";
            driver.findElement(By.linkText(textLogin)).click();


            driver.findElement(By.id("user_login"))
                    .sendKeys(user_login);
            driver.findElement(By.id("user_password"))
                    .sendKeys(password);
            driver.findElement(By.id("user_password_confirmation"))
                    .sendKeys(password);
            driver.findElement(By.id("user_firstname"))
                    .sendKeys(user_firstname);
            driver.findElement(By.id("user_lastname"))
                    .sendKeys(user_lastname);
            driver.findElement(By.id("user_mail"))
                    .sendKeys(user_mail);
            driver.findElement(By.name("commit")).click();


            //Email je neplatné
            //Přihlášení je již použito

            boolean exists = false;
            List<String> used = new ArrayList<>(Arrays.asList("Email je neplatné", "Přihlášení je již použito"));
            List<WebElement> countriesList = driver.findElements(By.tagName("li"));
            for (WebElement li : countriesList) {
                if (used.contains(li.getText())) {
                    exists = true;
                    System.out.println("Account already exists!");
                    return;
                }
            }

            if (!exists) {
                System.out.println("Account created");
            }


            //TODO
            //driver.findElement(By.linkText("Sign out")).click();
            driver.findElement(By.linkText("Odhlášení")).click();
        }

    }

    public void createProject(String theName, String salt) {

        String project_name = theName;
        String project_description = theName;
        String project_identifier = theName + salt;
        project_identifier = project_identifier.replaceAll(" ","");
        String project_homepage = "www." + theName + ".cz";


        driver.get("http://demo.redmine.org");

        //V mem prohlizeci je pouzita ceska lokalizace, mozna zamenit za cestinu za aj
        String textLogin = "Přihlášení";
        //String textLogin = "Sign in";
        driver.findElement(By.linkText(textLogin)).click();

        waitForElement(driver, By.id("username"));

        driver.findElement(By.id("username"))
                .sendKeys(user_login);
        driver.findElement(By.id("password"))
                .sendKeys(password);
        driver.findElement(By.name("login")).click();

        driver.findElement(By.linkText("Projekty")).click();

        driver.findElement(By.linkText("Nový projekt")).click();


        driver.findElement(By.id("project_name"))
                .sendKeys(project_name);


        driver.findElement(By.id("project_description"))
                .sendKeys(project_description);

        driver.findElement(By.id("project_identifier")).clear();
        driver.findElement(By.id("project_identifier"))
                .sendKeys(project_identifier);


        driver.findElement(By.id("project_homepage"))
                .sendKeys(project_homepage);


        driver.findElement(By.name("commit")).click();


    }

    public String getSalt(int count) {

        StringBuilder builder = new StringBuilder();

        while (count-- != 0) {
            Random r = new Random();
            int low = 0;
            int high = ALPHA_NUMERIC_STRING.length();
            int ran = r.nextInt(high - low) + low;

            builder.append(ALPHA_NUMERIC_STRING.charAt(ran));
        }

        return builder.toString();
    }



    @BeforeMethod(alwaysRun = true)
    public void setTestName(Method method) {
        currentTestName = method.getName();
    }

    public static RemoteWebDriver getDriver() {
        // https://www.guru99.com/gecko-marionette-driver-selenium.html
        // download from https://github.com/mozilla/geckodriver/releases
        //System.setProperty("webdriver.gecko.driver", "C:\\Users\\KSonn\\Dropbox\\CVUT\\ZKS\\seleniumukol\\mastering-selenium-testng_light-master\\geckodriver.exe");
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\KSonn\\Dropbox\\CVUT\\ZKS\\seleniumukol\\mastering-selenium-testng_light-master\\geckodriver.exe");
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        FirefoxOptions options = new FirefoxOptions();
        options.merge(desiredCapabilities);
        options.setHeadless(false);
        return new FirefoxDriver(options);
    }

    @AfterClass(alwaysRun = true)
    public void clearCookies() {
        try {
            driver.manage().deleteAllCookies();
        } catch (Exception ignored) {
            System.out.println("Unable to clear cookies, driver object is not viable...");
        }


        System.out.println("quit");
        driver.quit();
    }

    public static void waitForElement(WebDriver driver, final By by) {
        Wait<WebDriver> wait = new WebDriverWait(driver, 10);
        wait.until((ExpectedCondition<Boolean>) d -> d.findElement(by).isDisplayed());

        /*wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return driver.findElement(by).isDisplayed();
            }
        });*/
    }

    protected boolean waitForJSandJQueryToLoad(WebDriver driver) {

        WebDriverWait wait = new WebDriverWait(driver, 30);

        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver d) {
                try {
                    return ((Long) ((JavascriptExecutor) d).executeScript("return jQuery.active") == 0);
                } catch (Exception e) {
                    // no jQuery present
                    return true;
                }
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver d) {
                return ((JavascriptExecutor) d).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }
}