package com.masteringselenium.tests.kubelpe1;

import com.masteringselenium.DriverBase;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import org.testng.asserts.SoftAssert;


public class CRUDtesting extends DriverBase {



    @Test
    public void testCRUDNewProject() {
        String salt = getSalt(20);
        String project_name = "Novy projekt test web pole";
        String project_description = "Novy projekt test web pole";


        String project_identifier = salt;
        String project_homepage = "www.seznam.cz";


        driver.get("http://demo.redmine.org");

        String textLogin = "Přihlášení";
        //String textLogin = "Sign in";
        driver.findElement(By.linkText(textLogin)).click();

        waitForElement(driver, By.id("username"));

        driver.findElement(By.id("username"))
                .sendKeys(user_login);
        driver.findElement(By.id("password"))
                .sendKeys(password);
        driver.findElement(By.name("login")).click();

        driver.findElement(By.linkText("Projekty")).click();

        driver.findElement(By.linkText("Nový projekt")).click();


        //1-Create projekt
        driver.findElement(By.id("project_name"))
                .sendKeys(project_name);


        driver.findElement(By.id("project_description"))
                .sendKeys(project_description);


        driver.findElement(By.id("project_identifier")).clear();
        driver.findElement(By.id("project_identifier"))
                .sendKeys(project_identifier);


        driver.findElement(By.id("project_homepage"))
                .sendKeys(project_homepage);


        driver.findElement(By.name("commit")).click();

        String name;
        String description;
        String identifier;
        String homepage;


        //2- READ project
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();

        name = driver.findElement(By.id("project_name")).getAttribute("value");
        description = driver.findElement(By.id("project_description")).getAttribute("value");
        identifier = driver.findElement(By.id("project_identifier")).getAttribute("value");
        homepage = driver.findElement(By.id("project_homepage")).getAttribute("value");

        Assert.assertEquals(project_name, name);
        Assert.assertEquals(project_description, description);
        Assert.assertEquals(project_identifier, identifier);
        Assert.assertEquals(project_homepage, homepage);


        //3- Update website
        driver.findElement(By.id("project_homepage")).clear();
        driver.findElement(By.id("project_homepage"))
                .sendKeys("www.google.com");
        driver.findElement(By.name("commit")).click();


        //4 -Read website
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();
        homepage = driver.findElement(By.id("project_homepage")).getAttribute("value");
        Assert.assertEquals("www.google.com", homepage);
        //5 - Update name

        String salt2 = getSalt(10);
        driver.findElement(By.id("project_name")).clear();
        driver.findElement(By.id("project_name"))
                .sendKeys("Nove jmeno projektu" + salt2);
        driver.findElement(By.name("commit")).click();
        project_name = "Nove jmeno projektu" + salt2;

        //6 - Read name
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();
        name = driver.findElement(By.id("project_name")).getAttribute("value");
        Assert.assertEquals(name, project_name);


        //7 - Update description
        driver.findElement(By.id("project_description")).clear();
        driver.findElement(By.id("project_description"))
                .sendKeys("Novy popisek");
        driver.findElement(By.name("commit")).click();

        //8- read description
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();
        name = driver.findElement(By.id("project_description")).getAttribute("value");
        Assert.assertEquals(name, "Novy popisek");


        //9 - update verejny
        driver.findElement(By.id("project_is_public")).click();
        driver.findElement(By.name("commit")).click();

        //10 - read verejny
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();
        boolean a = !driver.findElement(By.id("project_is_public")).isSelected();
        Assert.assertTrue(a);


        //11 - DELETE KONEC
        driver.findElement(By.linkText("Přehled")).click();
        driver.findElement(By.linkText("Zavřít")).click();


        driver.switchTo().alert().accept();


        String result;
        boolean res = false;

        try {
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            result = driver.findElement(By.className("warning")).getText();
            res = true;

        } catch (org.openqa.selenium.NoSuchElementException ee) {
            //System.out.println(driver.getTitle());
            result = driver.findElement(By.id("errorExplanation")).getText();
        }

        Assert.assertTrue(res);


        driver.findElement(By.linkText("Odhlášení")).click();
    }


    @Test
    public void testCRUDNewTask() {
        boolean softassert = false;


        SoftAssert softAssertions = new SoftAssert();


        String issue_subject = "Poroouchalo se to";
        String issue_description = "Porouchla se to tak hodne ze potrebujeme tento system";
        String issue_estimated_hours = "50.0";
        String bug_or_feature = "Feature";
        String issue_start_date = "2019-06-06";


        // 1 - CREATE TASK
        String salt = getSalt(10);

        String project_name = "testing new task" + salt;

        System.out.println("testNewTask name: " + project_name);

        createProject(project_name, salt);

        driver.findElement(By.linkText("Projekty")).click();

        driver.findElement(By.linkText(project_name)).click();


        driver.findElement(By.linkText("Nový úkol")).click();


        Select dropDown = new Select(driver.findElement(By.id("issue_tracker_id")));
        dropDown.selectByVisibleText(bug_or_feature);


        driver.findElement(By.id("issue_subject"))
                .sendKeys(issue_subject);


        driver.findElement(By.id("issue_description"))
                .sendKeys(issue_description);


        driver.findElement(By.id("issue_estimated_hours")).clear();
        driver.findElement(By.id("issue_estimated_hours"))
                .sendKeys(issue_estimated_hours);


        driver.findElement(By.id("issue_start_date")).clear();
        driver.findElement(By.id("issue_start_date"))
                .sendKeys(issue_start_date);

        driver.findElement(By.name("commit")).click();

        // 2 - READ task created


        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Úkoly")).click();
        driver.findElement(By.linkText(issue_subject)).click();
        driver.findElement(By.linkText("Upravit")).click();


        String subject = driver.findElement(By.id("issue_subject")).getAttribute("value");
        String description = driver.findElement(By.id("issue_description")).getAttribute("value");
        String estimated_hours = driver.findElement(By.id("issue_estimated_hours")).getAttribute("value");


        Select dropDownn = new Select(driver.findElement(By.id("issue_tracker_id")));
        String dropDownval = dropDownn.getFirstSelectedOption().getText();

        String start_date = driver.findElement(By.id("issue_start_date")).getAttribute("value");

        Assert.assertEquals(issue_subject, subject);
        Assert.assertEquals(issue_description, description);
        Assert.assertEquals(issue_estimated_hours, estimated_hours);
        Assert.assertEquals(bug_or_feature, dropDownval);
        Assert.assertEquals(issue_start_date, start_date);

        // 3- UPDATE issue_estimated_hours
        driver.findElement(By.id("issue_estimated_hours")).clear();
        driver.findElement(By.id("issue_estimated_hours"))
                .sendKeys("10.0");


        driver.switchTo().activeElement().sendKeys(String.valueOf(Keys.ENTER));

        // 4- READ issue_estimated_hours


        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Úkoly")).click();
        driver.findElement(By.linkText(issue_subject)).click();

        estimated_hours = driver.findElement(By.id("issue_estimated_hours")).getAttribute("value");
        Assert.assertEquals(estimated_hours, "10.0");


        // 5- UPDATE issue_subject


        driver.findElement(By.linkText("Upravit")).click();

        driver.findElement(By.id("issue_subject")).clear();
        String newsalt = getSalt(10);
        issue_subject = "Novy subject" + newsalt;
        driver.findElement(By.id("issue_subject"))
                .sendKeys(issue_subject);

        driver.switchTo().activeElement().sendKeys(String.valueOf(Keys.ENTER));


        // 6- READ issue_subject

        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Úkoly")).click();
        driver.findElement(By.linkText(issue_subject)).click();

        subject = driver.findElement(By.id("issue_subject")).getAttribute("value");
        Assert.assertEquals(subject, issue_subject);


        // 7- UPDATE issue_description

        driver.findElement(By.linkText("Upravit")).click();

        WebElement temp = driver.findElement(By.xpath("//img[@src='/images/edit.png']"));
        temp.click();

        driver.findElement(By.id("issue_description")).clear();
        driver.findElement(By.id("issue_description"))
                .sendKeys("NOVY popis toho novehotasku");

        driver.switchTo().activeElement().sendKeys(String.valueOf(Keys.ENTER));

        // 8- READ issue_description

        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Úkoly")).click();
        driver.findElement(By.linkText(issue_subject)).click();

        description = driver.findElement(By.id("issue_description")).getAttribute("value");


        if (softassert) {
            softAssertions.assertEquals(description, "NOVY popis toho novehotasku");
        }

        //Assert.assertEquals(description,"NOVY popis toho novehotasku");


        // 9- UPDATE bug_or_feature
        driver.findElement(By.linkText("Upravit")).click();

        dropDown = new Select(driver.findElement(By.id("issue_tracker_id")));
        dropDown.selectByVisibleText("Support");

        driver.switchTo().activeElement().sendKeys(String.valueOf(Keys.ENTER));

        // 10- READ bug_or_feature

        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Úkoly")).click();
        driver.findElement(By.linkText(issue_subject)).click();

        driver.findElement(By.linkText("Upravit")).click();


        dropDownn = new Select(driver.findElement(By.id("issue_tracker_id")));
        dropDownval = dropDownn.getFirstSelectedOption().getText();


        if (softassert) {
            Assert.assertEquals(dropDownval, "Support");
        }

        // 11- UPDATE issue_start_date
        driver.findElement(By.linkText("Upravit")).click();


        driver.findElement(By.id("issue_start_date")).clear();
        driver.findElement(By.id("issue_start_date"))
                .sendKeys("2019-10-10");

        driver.switchTo().activeElement().sendKeys(String.valueOf(Keys.ENTER));


        // 12- READ issue_start_date

        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Úkoly")).click();
        driver.findElement(By.linkText(issue_subject)).click();

        driver.findElement(By.linkText("Upravit")).click();


        start_date = driver.findElement(By.id("issue_start_date")).getAttribute("value");


        Assert.assertEquals(start_date, "2019-10-10");


        // 13- UPDATE issue_estimated_hours
        driver.findElement(By.linkText("Upravit")).click();


        driver.findElement(By.id("issue_estimated_hours")).clear();
        driver.findElement(By.id("issue_estimated_hours"))
                .sendKeys("500.0");

        driver.switchTo().activeElement().sendKeys(String.valueOf(Keys.ENTER));

        // 14- READ issue_estimated_hours

        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Úkoly")).click();
        driver.findElement(By.linkText(issue_subject)).click();

        driver.findElement(By.linkText("Upravit")).click();


        estimated_hours = driver.findElement(By.id("issue_estimated_hours")).getAttribute("value");


        Assert.assertEquals(estimated_hours, "500.0");


        // 15- DELETE task
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText("Úkoly")).click();
        driver.findElement(By.linkText(issue_subject)).click();

        driver.findElement(By.linkText("Odstranit")).click();
        driver.switchTo().alert().accept();


        String result;
        boolean res = false;

        try {
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            //result = driver.findElement(By.className("nodata");
            WebElement ele = driver.findElementByCssSelector("p.nodata");
            res = true;


        } catch (org.openqa.selenium.NoSuchElementException e) {
            //System.out.println(driver.getTitle());
            result = driver.findElement(By.id("errorExplanation")).getText();
        }

        Assert.assertTrue(res);
        softAssertions.assertAll();


        driver.findElement(By.linkText("Odhlášení")).click();
    }


    @Test
    public void testNewSubProject() {


        String salt = getSalt(10);
        String sub_project_name = "subprojekt" + salt;
        String sub_project_description = "popis subprojekty";
        String sub_project_identifier = salt;
        String sub_project_homepage = "www.seznam.cz";


        String salt2 = getSalt(10);

        String project_name = "projekt" + salt2;
        System.out.println("testNewSubProject name: " + project_name);

        createProject(project_name, salt);


        driver.findElement(By.linkText("Projekty")).click();

        driver.findElement(By.linkText(project_name)).click();

        driver.findElement(By.linkText("Nový podprojekt")).click();


        //1. CREATE subprojekt
        driver.findElement(By.id("project_name")).clear();
        driver.findElement(By.id("project_name"))
                .sendKeys(sub_project_name);


        driver.findElement(By.id("project_description")).clear();
        driver.findElement(By.id("project_description"))
                .sendKeys(sub_project_description);


        driver.findElement(By.id("project_identifier")).clear();
        driver.findElement(By.id("project_identifier"))
                .sendKeys(sub_project_identifier);

        driver.findElement(By.id("project_homepage")).clear();
        driver.findElement(By.id("project_homepage"))
                .sendKeys(sub_project_homepage);


        driver.findElement(By.name("commit")).click();


        //2. read subprojekt
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText(sub_project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();

        String name = driver.findElement(By.id("project_name")).getAttribute("value");
        String description = driver.findElement(By.id("project_description")).getAttribute("value");
        String identifier = driver.findElement(By.id("project_identifier")).getAttribute("value");
        String homepage = driver.findElement(By.id("project_homepage")).getAttribute("value");

        Assert.assertEquals(sub_project_name, name);
        Assert.assertEquals(sub_project_description, description);
        Assert.assertEquals(sub_project_identifier, identifier);
        Assert.assertEquals(sub_project_homepage, homepage);


        //3- Update website
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText(sub_project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();


        driver.findElement(By.id("project_homepage")).clear();
        driver.findElement(By.id("project_homepage"))
                .sendKeys("www.google.com");


        driver.findElement(By.name("commit")).click();


        //4 -Read website
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText(sub_project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();


        homepage = driver.findElement(By.id("project_homepage")).getAttribute("value");
        Assert.assertEquals(homepage, "www.google.com");


        //5 - Update name
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText(sub_project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();


        String newsaltt = getSalt(10);
        sub_project_name = "projekt2" + newsaltt;

        driver.findElement(By.id("project_name")).clear();
        driver.findElement(By.id("project_name"))
                .sendKeys(sub_project_name);

        driver.findElement(By.name("commit")).click();


        //6 - Read name

        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText(sub_project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();


        name = driver.findElement(By.id("project_name")).getAttribute("value");

        Assert.assertEquals(name, sub_project_name);


        //7 - Update description
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText(sub_project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();


        driver.findElement(By.id("project_description")).clear();
        driver.findElement(By.id("project_description"))
                .sendKeys("novy popis");


        driver.findElement(By.name("commit")).click();


        //8- read description
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText(sub_project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();


        description = driver.findElement(By.id("project_description")).getAttribute("value");
        Assert.assertEquals(description, "novy popis");


        //9 - update verejny
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText(sub_project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();

        driver.findElement(By.id("project_is_public")).click();

        driver.findElement(By.name("commit")).click();


        //10 - read verejny
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText(sub_project_name)).click();
        driver.findElement(By.linkText("Nastavení")).click();

        boolean a = !driver.findElement(By.id("project_is_public")).isSelected();

        Assert.assertTrue(a);


        //11 - delete
        driver.findElement(By.linkText("Projekty")).click();
        driver.findElement(By.linkText(project_name)).click();
        driver.findElement(By.linkText(sub_project_name)).click();

        driver.findElement(By.linkText("Zavřít")).click();
        driver.switchTo().alert().accept();


        String result;
        boolean res = false;

        try {
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            result = driver.findElement(By.className("warning")).getText();
            res = true;

        } catch (org.openqa.selenium.NoSuchElementException ee) {
            //System.out.println(driver.getTitle());
            result = driver.findElement(By.id("errorExplanation")).getText();
        }

        Assert.assertTrue(res);


        driver.findElement(By.linkText("Odhlášení")).click();

    }


}
