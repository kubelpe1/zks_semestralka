package com.masteringselenium.tests.kubelpe1;

import com.masteringselenium.DriverBase;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;
import java.util.List;


//import org.assertj.core.api.SoftAssertions;

import org.testng.asserts.SoftAssert;
import org.testng.annotations.Test;


public class EquivalenceTesting extends DriverBase {


    @Test
    public void testNewTaskDate() {

        ArrayList<String> options = new ArrayList<>(Arrays.asList("2018-01-02", "2018-01-0", "2019-06-06", "2019-13-06", "-50", "DROP TABLE;", "2019-06-13"));
        ArrayList<Boolean> results = new ArrayList<>(Arrays.asList(true, false, true, false, false, false, true));

        SoftAssert softAssertions = new SoftAssert();

        for (int iteration = 0; iteration < options.size(); iteration++) {

            boolean grandTrue = results.get(iteration);

            String issue_subject = "Poroouchalo se to";
            String issue_description = "Porouchla se to tak hodne ze potrebujeme tento system";

            String issue_estimated_hours = options.get(iteration);
            String bug_or_feature = "Feature";


            String salt = getSalt(10);

            String project_name = "testing new task" + salt;

            System.out.println("testNewTask name: " + project_name);

            createProject(project_name, salt);

            driver.findElement(By.linkText("Projekty")).click();

            driver.findElement(By.linkText(project_name)).click();


            driver.findElement(By.linkText("Nový úkol")).click();


            Select dropDown = new Select(driver.findElement(By.id("issue_tracker_id")));
            dropDown.selectByVisibleText(bug_or_feature);


            driver.findElement(By.id("issue_subject"))
                    .sendKeys(issue_subject);


            driver.findElement(By.id("issue_description"))
                    .sendKeys(issue_description);


            driver.findElement(By.id("issue_start_date")).clear();
            driver.findElement(By.id("issue_start_date"))
                    .sendKeys(options.get(iteration));
            String textafterinput = driver.findElement(By.id("issue_start_date")).getAttribute("value");

            if (textafterinput.length() == 0) {
                grandTrue = true;
            }


            driver.findElement(By.name("commit")).click();


            String result;
            boolean res = false;

            try {
                result = driver.findElement(By.id("flash_notice")).getText();
                res = true;

            } catch (org.openqa.selenium.NoSuchElementException e) {
                //System.out.println(driver.getTitle());
                result = driver.findElement(By.id("errorExplanation")).getText();
            }


            softAssertions.assertEquals(res, grandTrue);


            System.out.println("input: " + issue_estimated_hours + " iteration " + iteration + " OK" + " " + res + " " + grandTrue);
            driver.findElement(By.linkText("Odhlášení")).click();


        }

        softAssertions.assertAll();
    }


    @Test
    public void testNewProjectHomepage() {
        ArrayList<String> options = new ArrayList<>(Arrays.asList("www.seznam", "....", "www.seznam.cz", "asd5445asd"));
        ArrayList<Boolean> results = new ArrayList<>(Arrays.asList(false, false, true, false));

        SoftAssert softAssertions = new SoftAssert();


        for (int iteration = 0; iteration < options.size(); iteration++) {

            String salt = getSalt(20);
            String project_name = "Novy projekt test web pole";
            String project_description = "Novy projekt test web pole";


            String project_identifier = salt;
            String project_homepage = options.get(3);


            driver.get("http://demo.redmine.org");

            //V mem prohlizeci je pouzita ceska lokalizace, mozna zamenit za cestinu za aj
            String textLogin = "Přihlášení";
            //String textLogin = "Sign in";
            driver.findElement(By.linkText(textLogin)).click();

            waitForElement(driver, By.id("username"));

            driver.findElement(By.id("username"))
                    .sendKeys(user_login);
            driver.findElement(By.id("password"))
                    .sendKeys(password);
            driver.findElement(By.name("login")).click();

            driver.findElement(By.linkText("Projekty")).click();

            driver.findElement(By.linkText("Nový projekt")).click();


            driver.findElement(By.id("project_name"))
                    .sendKeys(project_name);


            driver.findElement(By.id("project_description"))
                    .sendKeys(project_description);


            driver.findElement(By.id("project_identifier")).clear();
            driver.findElement(By.id("project_identifier"))
                    .sendKeys(project_identifier);


            driver.findElement(By.id("project_homepage"))
                    .sendKeys(project_homepage);


            driver.findElement(By.name("commit")).click();


            String result;
            boolean res = false;

            try {
                result = driver.findElement(By.id("flash_notice")).getText();
                res = result.equals("Úspěšně vytvořeno.");

            } catch (org.openqa.selenium.NoSuchElementException e) {
                result = driver.findElement(By.id("errorExplanation")).getText();
            }


            softAssertions.assertEquals(res, (boolean) results.get(iteration));

            driver.findElement(By.linkText("Odhlášení")).click();
        }
        softAssertions.assertAll();
    }


    @Test
    public void testNewTaskHours() {

        ArrayList<String> options = new ArrayList<>(Arrays.asList("-50", "dasd", "50", "0", "sda4sda4", "DROP TABLE"));
        ArrayList<Boolean> results = new ArrayList<>(Arrays.asList(false, false, true, true, false, false));

        SoftAssert softAssertions = new SoftAssert();

        for (int iteration = 0; iteration < options.size(); iteration++) {


            String issue_subject = "Poroouchalo se to";
            String issue_description = "Porouchla se to tak hodne ze potrebujeme tento system";

            String issue_estimated_hours = options.get(iteration);
            String bug_or_feature = "Feature";


            String salt = getSalt(10);

            String project_name = "testing new task" + salt;

            System.out.println("testNewTask name: " + project_name);

            createProject(project_name, salt);

            driver.findElement(By.linkText("Projekty")).click();

            driver.findElement(By.linkText(project_name)).click();


            driver.findElement(By.linkText("Nový úkol")).click();


            Select dropDown = new Select(driver.findElement(By.id("issue_tracker_id")));
            dropDown.selectByVisibleText(bug_or_feature);


            driver.findElement(By.id("issue_subject"))
                    .sendKeys(issue_subject);


            driver.findElement(By.id("issue_description"))
                    .sendKeys(issue_description);


            driver.findElement(By.id("issue_estimated_hours"))
                    .sendKeys(issue_estimated_hours);


            driver.findElement(By.name("commit")).click();


            String result;
            boolean res = false;

            try {


                WebElement hours = driver.findElement(By.xpath("//td[@class='estimated-hours']"));
                String txt = hours.getText();

                res = true;


            } catch (org.openqa.selenium.NoSuchElementException e) {

                res = !results.get(iteration);
            }


            softAssertions.assertTrue(res);


            driver.findElement(By.linkText("Odhlášení")).click();


        }

        softAssertions.assertAll();
    }


}
