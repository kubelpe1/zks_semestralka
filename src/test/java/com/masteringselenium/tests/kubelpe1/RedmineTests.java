package com.masteringselenium.tests.kubelpe1;

import com.masteringselenium.DriverBase;
import com.masteringselenium.tests.kubelpe1.JSONInputs.NewNewsInput;
import com.masteringselenium.tests.kubelpe1.JSONInputs.NewProjectInput;
import com.masteringselenium.tests.kubelpe1.JSONInputs.NewSubProjectInput;
import com.masteringselenium.tests.kubelpe1.JSONInputs.NewTaskInput;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.*;


public class RedmineTests extends DriverBase {


    @Test
    public void createProfile() {

        String user_firstname = "Peta";
        String user_lastname = "Kubelka";

        driver.get("http://demo.redmine.org");


        String textLogin = "Registrovat";
        driver.findElement(By.linkText(textLogin)).click();


        driver.findElement(By.id("user_login"))
                .sendKeys(user_login);
        driver.findElement(By.id("user_password"))
                .sendKeys(password);
        driver.findElement(By.id("user_password_confirmation"))
                .sendKeys(password);
        driver.findElement(By.id("user_firstname"))
                .sendKeys(user_firstname);
        driver.findElement(By.id("user_lastname"))
                .sendKeys(user_lastname);
        driver.findElement(By.id("user_mail"))
                .sendKeys(user_mail);
        driver.findElement(By.name("commit")).click();


        List<String> used = new ArrayList<>(Arrays.asList("Email je neplatné", "Přihlášení je již použito"));
        List<WebElement> countriesList = driver.findElements(By.tagName("li"));
        for (WebElement li : countriesList) {
            if (used.contains(li.getText())) {
                System.out.println("Account already exists!");
                return;
            }
        }

        System.out.println("Account created");
        waitForElement(driver, By.id("loggedas"));
        WebElement loggedAs = driver.findElement(By.id("loggedas"));

        String loggedText = loggedAs.getText();

        Assert.assertEquals(loggedText, "Přihlášen jako " + user_login);

        driver.findElement(By.linkText("Odhlášení")).click();
    }


    @Test
    public void testNewProject() {

        ArrayList<String> options = new ArrayList<>(Arrays.asList("test New Project", "testing creating new project", "testnewprojectidentifier", "www.testingmyproject.cz"));

        NewProjectInput npi = null;
        try {


            npi = new NewProjectInput(options, "test_project_mcc.json");

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }


        for (int iteration = 0; iteration < npi.getInputs().size(); iteration++) {

            String salt = getSalt(20);
            String project_name = npi.getInputs().get(iteration).getInput().get(0);
            String project_description = npi.getInputs().get(iteration).getInput().get(1);

            String id = npi.getInputs().get(iteration).getInput().get(2);
            String project_identifier = (id.length() == 0) ? "" : id + salt;
            String project_homepage = npi.getInputs().get(iteration).getInput().get(3);


            driver.get("http://demo.redmine.org");


            String textLogin = "Přihlášení";

            driver.findElement(By.linkText(textLogin)).click();

            waitForElement(driver, By.id("username"));

            driver.findElement(By.id("username"))
                    .sendKeys(user_login);
            driver.findElement(By.id("password"))
                    .sendKeys(password);
            driver.findElement(By.name("login")).click();

            driver.findElement(By.linkText("Projekty")).click();

            driver.findElement(By.linkText("Nový projekt")).click();


            driver.findElement(By.id("project_name"))
                    .sendKeys(project_name);


            driver.findElement(By.id("project_description"))
                    .sendKeys(project_description);


            driver.findElement(By.id("project_identifier")).clear();
            driver.findElement(By.id("project_identifier"))
                    .sendKeys(project_identifier);


            driver.findElement(By.id("project_homepage"))
                    .sendKeys(project_homepage);


            driver.findElement(By.name("commit")).click();


            String result;
            boolean res = false;

            try {
                result = driver.findElement(By.id("flash_notice")).getText();
                res = result.equals("Úspěšně vytvořeno.");

            } catch (org.openqa.selenium.NoSuchElementException e) {
                result = driver.findElement(By.id("errorExplanation")).getText();
            }


            Assert.assertEquals(res, npi.getInputs().get(iteration).resultValue());


            System.out.println("input: " + iteration + " OK" + " " + res + " " + npi.getInputs().get(iteration).resultValue());

            driver.findElement(By.linkText("Odhlášení")).click();
        }

    }

    @Test
    public void testLoginSuccessful() {


        driver.get("http://demo.redmine.org");

        for (int i = 0; i < 2; i++) {

            String log = (i == 0) ? user_login : "";
            String pass = (i == 0) ? "" : password;


            String textLogin = "Přihlášení";

            driver.findElement(By.linkText(textLogin)).click();

            driver.findElement(By.id("username")).clear();
            driver.findElement(By.id("password")).clear();

            waitForElement(driver, By.id("username"));

            driver.findElement(By.id("username"))
                    .sendKeys(log);
            driver.findElement(By.id("password"))
                    .sendKeys(pass);
            driver.findElement(By.name("login")).click();


            WebElement loggedAs = driver.findElement(By.id("flash_error"));

            String loggedText = loggedAs.getText();
            Assert.assertEquals(loggedText, "Chybné jméno nebo heslo");


        }
    }

    @Test
    public void testNewTask() {

        ArrayList<String> options = new ArrayList<>(Arrays.asList("Poroouchalo se to",
                "Porouchla se to tak hodne ze potrebujeme tento system",
                "50",
                "Bug Feature"));
        NewTaskInput npi = null;
        try {

            npi = new NewTaskInput(options, "test_new_task_pairwise.json");

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }


        for (int iteration = 0; iteration < npi.getInputs().size(); iteration++) {


            String issue_subject = npi.getInputs().get(iteration).getInput().get(0);
            String issue_description = npi.getInputs().get(iteration).getInput().get(1);

            String issue_estimated_hours = npi.getInputs().get(iteration).getInput().get(2);
            String bug_or_feature = npi.getInputs().get(iteration).getInput().get(3);


            String salt = getSalt(10);

            String project_name = "testing new task" + salt;

            System.out.println("testNewTask name: " + project_name);

            createProject(project_name, salt);

            driver.findElement(By.linkText("Projekty")).click();

            driver.findElement(By.linkText(project_name)).click();


            driver.findElement(By.linkText("Nový úkol")).click();


            Select dropDown = new Select(driver.findElement(By.id("issue_tracker_id")));
            dropDown.selectByVisibleText(bug_or_feature);


            driver.findElement(By.id("issue_subject"))
                    .sendKeys(issue_subject);


            driver.findElement(By.id("issue_description"))
                    .sendKeys(issue_description);


            driver.findElement(By.id("issue_estimated_hours"))
                    .sendKeys(issue_estimated_hours);


            driver.findElement(By.name("commit")).click();


            String result;
            boolean res = false;

            try {
                result = driver.findElement(By.id("flash_notice")).getText();
                res = true;

            } catch (org.openqa.selenium.NoSuchElementException e) {
                result = driver.findElement(By.id("errorExplanation")).getText();
            }


            Assert.assertEquals(res, npi.getInputs().get(iteration).resultValue());


            System.out.println("input: " + iteration + " OK" + " " + res + " " + npi.getInputs().get(iteration).resultValue());
            driver.findElement(By.linkText("Odhlášení")).click();


        }
    }


    @Test
    public void testNewNews() {


        ArrayList<String> options = new ArrayList<>(Arrays.asList("test New Project", "testing creating new project", "testnewprojectidentifier", "www.testingmyproject.cz"));

        NewNewsInput npi = null;
        try {
            npi = new NewNewsInput(options, "test_new_news.json");

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }


        for (int iteration = 0; iteration < npi.getInputs().size(); iteration++) {


            String news_title = npi.getInputs().get(iteration).getInput().get(0);
            String news_summary = npi.getInputs().get(iteration).getInput().get(1);
            String news_description = npi.getInputs().get(iteration).getInput().get(2);

            String salt = getSalt(10);

            String project_name = "testing new news" + salt;


            System.out.println("testNewNews name: " + project_name);
            createProject(project_name, salt);


            driver.findElement(By.linkText("Projekty")).click();

            driver.findElement(By.linkText(project_name)).click();


            driver.findElement(By.linkText("Novinky")).click();

            driver.findElement(By.linkText("Přidat novinku")).click();


            driver.findElement(By.id("news_title"))
                    .sendKeys(news_title);


            driver.findElement(By.id("news_summary"))
                    .sendKeys(news_summary);


            driver.findElement(By.id("news_description"))
                    .sendKeys(news_description);


            driver.findElement(By.name("commit")).click();

            String result;
            boolean res = false;

            try {
                result = driver.findElement(By.id("flash_notice")).getText();
                res = result.equals("Úspěšně vytvořeno.");

            } catch (org.openqa.selenium.NoSuchElementException e) {
                result = driver.findElement(By.id("errorExplanation")).getText();
            }


            Assert.assertEquals(res, npi.getInputs().get(iteration).resultValue());


            System.out.println("input: " + iteration + " OK" + " " + res + " " + npi.getInputs().get(iteration).resultValue());


            driver.findElement(By.linkText("Odhlášení")).click();


        }

    }


    @Test
    public void testNewSubProject() {

        ArrayList<String> options = new ArrayList<>(Arrays.asList("Poroouchalo se to",
                "Porouchla se to tak hodne ze potrebujeme tento system",
                "50",
                "Bug Feature"));
        NewSubProjectInput npi = null;
        try {
            npi = new NewSubProjectInput(options, "test_new_subproject_mcdc.json");

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }


        for (int iteration = 0; iteration < npi.getInputs().size(); iteration++) {

            String salt = getSalt(10);
            String sub_project_name = npi.getInputs().get(iteration).getInput().get(0) + salt;

            String sub_project_description = npi.getInputs().get(iteration).getInput().get(1);

            String id = npi.getInputs().get(iteration).getInput().get(2);
            String sub_project_identifier = (id.length() == 0) ? "" : id + salt;


            String salt2 = getSalt(10);


            String project_name = "testing new sub" + salt2;


            System.out.println("testNewSubProject name: " + project_name);

            createProject(project_name, salt);


            driver.findElement(By.linkText("Projekty")).click();

            driver.findElement(By.linkText(project_name)).click();

            driver.findElement(By.linkText("Nový podprojekt")).click();


            driver.findElement(By.id("project_name")).clear();
            driver.findElement(By.id("project_name"))
                    .sendKeys(sub_project_name);


            driver.findElement(By.id("project_description")).clear();
            driver.findElement(By.id("project_description"))
                    .sendKeys(sub_project_description);


            driver.findElement(By.id("project_identifier")).clear();
            driver.findElement(By.id("project_identifier"))
                    .sendKeys(sub_project_identifier);


            driver.findElement(By.name("commit")).click();


            String result;
            boolean res = false;

            try {
                result = driver.findElement(By.id("flash_notice")).getText();
                res = result.equals("Úspěšně vytvořeno.");

            } catch (org.openqa.selenium.NoSuchElementException e) {
                result = driver.findElement(By.id("errorExplanation")).getText();
            }


            Assert.assertEquals(res, npi.getInputs().get(iteration).resultValue());


            System.out.println("input: " + iteration + " OK" + " " + res + " " + npi.getInputs().get(iteration).resultValue());
            driver.findElement(By.linkText("Odhlášení")).click();

        }
    }


    @Test
    public void testNewDocument() {


        for (int iteration = 0; iteration < 2; iteration++) {


            String document_title = (iteration == 0) ? "Toto je novy document" : "";
            String document_description = (iteration == 0) ? "Popis noveho dokumentu" : "";
            String dropdown1 = "User documentation";
            String dropdown2 = "Technical documentation";
            String dropdown = (iteration == 0) ? dropdown1 : dropdown2;
            boolean resultAssert = (iteration == 0);

            String salt = getSalt(10);
            String project_name = "testing new doc" + salt;


            System.out.println("testNewDocument name: " + project_name);


            createProject(project_name, salt);


            driver.findElement(By.linkText("Projekty")).click();

            driver.findElement(By.linkText(project_name)).click();


            driver.findElement(By.linkText("Dokumenty")).click();

            driver.findElement(By.linkText("Nový dokument")).click();


            Select dropDownCategory = new Select(driver.findElement(By.id("document_category_id")));


            dropDownCategory.selectByVisibleText(dropdown);


            driver.findElement(By.id("document_title"))
                    .sendKeys(document_title);


            driver.findElement(By.id("document_description"))
                    .sendKeys(document_description);


            driver.findElement(By.name("commit")).click();


            String result;
            boolean res = false;

            try {
                result = driver.findElement(By.id("flash_notice")).getText();
                res = result.equals("Úspěšně vytvořeno.");

            } catch (org.openqa.selenium.NoSuchElementException e) {
                result = driver.findElement(By.id("errorExplanation")).getText();
            }


            Assert.assertEquals(res, resultAssert);


            driver.findElement(By.linkText("Odhlášení")).click();
        }

    }


    @Test
    public void testAddWiki() {
        for (int iteration = 0; iteration < 2; iteration++) {


            String context_text = "Wiki stranka";
            String content_comments = (iteration == 0) ? "komentar k wiki " : "";
            boolean resultAssert = (iteration == 0);

            String salt = getSalt(10);
            String project_name = "testing add wiki" + salt;
            System.out.println("testNewTask name: " + project_name);
            createProject(project_name, salt);


            driver.findElement(By.linkText("Projekty")).click();

            driver.findElement(By.linkText(project_name)).click();


            driver.findElement(By.linkText("Wiki")).click();

            driver.findElement(By.id("content_text")).click();

            WebElement element = driver.findElement(By.id("content_text"));
            element.clear();
            element.sendKeys(context_text);


            WebElement element2 = driver.findElement(By.id("content_comments"));
            element2.clear();
            element2.sendKeys(content_comments);


            driver.findElement(By.name("commit")).click();


            WebElement result;
            boolean res = false;

            try {

                WebElement elementt = driver.findElement(By.xpath("//div[@class='wiki wiki-page']"));

                res = true;


            } catch (org.openqa.selenium.NoSuchElementException e) {
                System.out.println(e);

            }


            Assert.assertTrue(res);


            driver.findElement(By.linkText("Odhlášení")).click();
        }


    }


}
