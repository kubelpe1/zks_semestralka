package com.masteringselenium.tests.kubelpe1.JSONInputs;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.File;
import java.util.ArrayList;

public class NewSubProjectInput {
    //private ArrayList<ArrayList<String>> inputs;
    private ArrayList<PairInputAndResult> inputs = new ArrayList<>();

    public NewSubProjectInput(ArrayList<String> options,String path) throws IOException, ParseException {

        String filePath = new File("").getAbsolutePath();
        filePath += "\\src\\test\\java\\com\\masteringselenium\\jsons\\" + path;



        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader(filePath));

            JSONArray jsonArr = (JSONArray) obj;


            for(int n = 0; n < jsonArr.size(); n++)
            {
                JSONObject a = (JSONObject) jsonArr.get(n);
                //{"project_description":false,"project_homepage":false,"project_name":false,"project_identifier":false}
                boolean sub_project_name = (boolean) a.get("sub_project_name");
                boolean sub_project_description = (boolean) a.get("sub_project_description");
                boolean sub_project_identifier = (boolean) a.get("sub_project_identifier");


                ArrayList<String> list = new ArrayList<>();

                list.add((sub_project_name) ? options.get(0) : "");
                list.add((sub_project_description) ? options.get(1) : "");
                list.add((sub_project_identifier) ? options.get(2) : "");




                inputs.add(new PairInputAndResult(list,  sub_project_identifier));

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public ArrayList<PairInputAndResult> getInputs() {
        return inputs;
    }

    public void setInputs(ArrayList<PairInputAndResult> inputs) {
        this.inputs = inputs;
    }
}

