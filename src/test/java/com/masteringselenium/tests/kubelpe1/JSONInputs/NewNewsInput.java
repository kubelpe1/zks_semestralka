package com.masteringselenium.tests.kubelpe1.JSONInputs;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.File;
import java.util.ArrayList;

public class NewNewsInput {
    private ArrayList<PairInputAndResult> inputs = new ArrayList<>();

    public NewNewsInput(ArrayList<String> options, String path) throws IOException, ParseException {

        String filePath = new File("").getAbsolutePath();
        filePath += "\\src\\test\\java\\com\\masteringselenium\\jsons\\" + path;




        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader(filePath));

            JSONArray jsonArr = (JSONArray) obj;


            for(int n = 0; n < jsonArr.size(); n++)
            {
                JSONObject a = (JSONObject) jsonArr.get(n);
                boolean news_title = (boolean) a.get("news_title");
                boolean news_summary = (boolean) a.get("news_summary");
                boolean news_description = (boolean) a.get("news_description");

                ArrayList<String> list = new ArrayList<>();

                list.add((news_title) ? options.get(0) : "");
                list.add((news_summary) ? options.get(1) : "");
                list.add((news_description) ?  options.get(2) : "");




                inputs.add(new PairInputAndResult(list,  ( news_title && news_description)));

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public ArrayList<PairInputAndResult> getInputs() {
        return inputs;
    }

    public void setInputs(ArrayList<PairInputAndResult> inputs) {
        this.inputs = inputs;
    }
}

