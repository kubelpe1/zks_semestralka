package com.masteringselenium.tests.kubelpe1.JSONInputs;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.File;
import java.util.ArrayList;

public class NewTaskInput {
    //private ArrayList<ArrayList<String>> inputs;
    private ArrayList<PairInputAndResult> inputs = new ArrayList<>();

    public NewTaskInput(ArrayList<String> options,String path) throws IOException, ParseException {

        String filePath = new File("").getAbsolutePath();
        filePath += "\\src\\test\\java\\com\\masteringselenium\\jsons\\" + path;



        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader(filePath));

            JSONArray jsonArr = (JSONArray) obj;


            for(int n = 0; n < jsonArr.size(); n++)
            {
                JSONObject a = (JSONObject) jsonArr.get(n);
                //{"project_description":false,"project_homepage":false,"project_name":false,"project_identifier":false}
                boolean issue_subject = (boolean) a.get("issue_subject");
                boolean issue_description = (boolean) a.get("issue_description");
                boolean issue_estimated_hours = (boolean) a.get("issue_estimated_hours");
                boolean bug_or_feature = (boolean) a.get("bug_or_feature");

                ArrayList<String> list = new ArrayList<>();

                list.add((issue_subject) ? options.get(0) : "");
                list.add((issue_description) ? options.get(1) : "");
                list.add((issue_estimated_hours) ? options.get(2) : "");

                String [] split = options.get(3).split(" ");
                list.add((bug_or_feature) ?  split[0] : split[1]);




                inputs.add(new PairInputAndResult(list,  issue_subject));

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public ArrayList<PairInputAndResult> getInputs() {
        return inputs;
    }

    public void setInputs(ArrayList<PairInputAndResult> inputs) {
        this.inputs = inputs;
    }
}

