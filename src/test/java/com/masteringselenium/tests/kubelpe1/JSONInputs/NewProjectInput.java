package com.masteringselenium.tests.kubelpe1.JSONInputs;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.File;
import java.util.ArrayList;

public class NewProjectInput {
    private ArrayList<PairInputAndResult> inputs = new ArrayList<>();

    public NewProjectInput(ArrayList<String> options, String path) throws IOException, ParseException {

        String filePath = new File("").getAbsolutePath();
        filePath += "\\src\\test\\java\\com\\masteringselenium\\jsons\\" + path;



        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader(filePath));

            JSONArray jsonArr = (JSONArray) obj;


            for(int n = 0; n < jsonArr.size(); n++)
            {
                JSONObject a = (JSONObject) jsonArr.get(n);
                boolean project_description = (boolean) a.get("project_description");
                boolean project_homepage = (boolean) a.get("project_homepage");
                boolean project_name = (boolean) a.get("project_name");
                boolean project_identifier = (boolean) a.get("project_identifier");

                ArrayList<String> list = new ArrayList<>();
                list.add((project_name) ? options.get(0) : "");
                list.add((project_description) ? options.get(1) : "");
                list.add((project_identifier) ? options.get(2) : "");
                list.add((project_homepage) ? options.get(3) : "");



                inputs.add(new PairInputAndResult(list,  (project_name && project_identifier)));

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public ArrayList<PairInputAndResult> getInputs() {
        return inputs;
    }

    public void setInputs(ArrayList<PairInputAndResult> inputs) {
        this.inputs = inputs;
    }
}
