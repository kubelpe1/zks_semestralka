package com.masteringselenium.tests.kubelpe1.JSONInputs;

import java.util.ArrayList;

/**
 * Created by KSonn on 03.01.2019.
 */
public class PairInputAndResult {
    ArrayList<String> input;
    boolean result;

    public PairInputAndResult(ArrayList<String> input, boolean result) {
        this.input = input;
        this.result = result;
    }

    public ArrayList<String> getInput() {
        return input;
    }

    public void setInput(ArrayList<String> input) {
        this.input = input;
    }

    public boolean resultValue() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
